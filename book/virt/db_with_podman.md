
## MariaDB

```bash

podman create -p 3306:3306 --name maria_db \
  -v $(pwd)/dbs:/mnt/data:z \
  -e MARIADB_ROOT_PASSWORD=mypass \
  docker.io/library/mariadb:slim
podman start maria_db

```

## postgreDB

### with pull image

```bash

# PART 1: Pull image
# search image
podman search postgres
# docker.io/library/postgres

# pull image
podman pull docker.io/library/postgres:slim

# verify if image dawnloaded
podman images

# PART 2: Run container
podman run -dt --name postgre_db \
-e POSTGRES_PASSWORD=1234 \
-v "/home/dbs/postgreDb:/var/lib/postgresql/data" -p 5432:5432 postgres

# stop container 
podman stop _container_name
```

### with create pod

```bash

# create new pod
podman pod create --name postgre_sql -p 5432 -p 9187

# run container
podman run -d --pod postgre_sql \
-e POSTGRES_USER=admin \
-e POSTGRES_PASSWORD=password \
postgres:latest

# or 
podman run -d --pod postgre_sql \
-e DATA_SOURCE_NAME="postgresql://postgres:password@localhost:5432/postgres?sslmode=disable" \
wrouesnel/postgres_exporter

# access PostgreSQL metrics
localhost:9187/metrics
```

## persistent data storage volume

```bash
# Create a persistent data storage volume
podman volume create static

# verify volume
podman volume ls

# Inspect the volume details
podman volume inspect static

# delete volume
podman volume rm static
```