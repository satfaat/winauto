## Podman 

src
- https://github.com/containers/podman/blob/main/docs/tutorials/podman-for-windows.md
- https://podman.io/docs

```bash
podman --version

podman machine init
podman machine start

#test 
podman run ubi8-micro date
podman run --rm -d -p 8080:80 --name httpd docker.io/library/httpd

# List pods
podman pod ps 

# Remove all stopped pods and their containers
prune
```

## Volume Mounting

```bash
# window style path 
podman run --rm -v c:\Users\User\myfolder:/myfolder ubi8-micro ls /myfolder

# unixy windows path
podman run --rm -v /c/Users/User/myfolder:/myfolder ubi8-micro ls /myfolder

# linux path
podman run --rm -v /var/myfolder:/myfolder ubi-micro ls /myfolder

```

## Listing Podman Machine(s)

```bash
podman machine ls
```


## Images

- image > container

```bash

# search 
podman search _image_name

# search with filter
podman search httpd --filter=is-official

# pulling an image
podman pull docker.io/library/httpd
podman pull docker.io/oven/bun
podman pull docker.io/oven/bun:slim

# list all images
podman images
```

## Containers

```bash
# run conteiner
podman run -dt -p 8080:80/tcp docker.io/library/httpd
podman run --rm --init --ulimit memlock=-1:-1 oven/bun

# all containers
podman ps
```
