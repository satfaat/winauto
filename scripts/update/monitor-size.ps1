# Retrieve monitor information
$monitors = Get-WmiObject -Namespace root\cimv2 -Class Win32_DesktopMonitor

# Iterate through each monitor and display its size
foreach ($monitor in $monitors) {
    $monitorSize = "{0} inches" -f $monitor.ScreenHeight / 2.54
    Write-Host "Monitor Size: $monitorSize"
}